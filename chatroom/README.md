# Client


## Pagina principale

Tramite la pagina principale è possibile visualizzare lo stato online/offline del client e il tasto "log out" per disconnettersi dal server.


In fondo alla pagina c'è un TextField per inserimento del messaggio.


Con uno swipe verso destra da sinistra si può aprire la pagina di login.


In essa si inserisce l'indirizzo ip del server e l'username.

Ho scelto di utilizzare un TextField in cui l'utente può specificare l'ip del server perchè client e server devono essere collegati alla stessa rete, e l'indirizzo ip del pc che fungerà
da server può variare in base alla rete.


----------------

### Server

Il server apre una connessione con il client e risponde alle richieste di esso.

Inoltre invia il client la lista e il numero degli utenti conneessi nella chat

Principalmente distribusice i messaggi a tutti i client connessi