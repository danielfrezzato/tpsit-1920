import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';

import 'package:device_info/device_info.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat',
      debugShowCheckedModeBanner: false,
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      home: MyHomePage(title: 'Chat'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MaterialColor statoColore = Colors.red;
  String stato = "Offline";
  String username = "User";
  String avatar = " ";
  String ip = " ";
  String message = " ";
  List<String> listMessages = <String>[""];

  TextEditingController ipController = new TextEditingController(text: '');
  TextEditingController usernameController = new TextEditingController();
  TextEditingController msgController = new TextEditingController();

  Socket socket;

  final LocalAuthentication _localAuthentication = LocalAuthentication();
  
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
  

  bool impronta = false;

  void userlogin() {
    if (impronta == false) {
      setState(() {
        ip = ipController.text;
        username = usernameController.text;
        avatar = usernameController.text.substring(0, 1);
      });
    } else {
      setState(() {
        ip = ipController.text;
        username = androidInfo.getName();
        avatar = iniz;
      });
    }

    Socket.connect(ipController.text, 3000).then((Socket socketConnect) {
      setState(() {
        stato = "Online";
        statoColore = Colors.green;
      });
      socket = socketConnect;
      listMessages.removeLast();
      socket.write("$username e' online.\n");
      socket.listen(
        (List<int> data) {
          setState(() {
            listMessages.insert(0, String.fromCharCodes(data).trim());
          });
        },
      );
    });
  }

  Future<void> _authorizeNow() async {
    bool isAuthorized = false;
    try {
      isAuthorized = await _localAuthentication.authenticateWithBiometrics(
        localizedReason: "Inserire impronta digitale",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      print(e);
    }

    if (!mounted) return;

    setState(() {
      if (isAuthorized) {
        impronta = true;
        userlogin();
      }
    });
  }

  void send() {
    message = msgController.text;
    if (message != "") {
      socket.write("$username: $message\n");
      setState(() {
        listMessages.insert(0, "Tu: $message");
      });
      msgController.clear();
    }
  }

  void disconnect() {
    socket.write("$username e' offline.\n");
    socket.close();
    socket = null;
    setState(() {
      username="";
      avatar="";
      ip="";
      statoColore = Colors.red;
      stato = "Offline";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          Center(
            child: Text(
              "$stato",
              style: TextStyle(color: statoColore),
            ),
          ),
          RaisedButton(
            onPressed: disconnect,
            color: Colors.blue,
            child: const Text('Log out', style: TextStyle(fontSize: 13)),
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("$username"),
              accountEmail: Text("$ip"),
              currentAccountPicture: CircleAvatar(
                backgroundColor:
                    Theme.of(context).platform == TargetPlatform.iOS
                        ? Colors.black
                        : Colors.white,
                child: Text(
                  "$avatar",
                  style: TextStyle(fontSize: 40.0),
                ),
              ),
            ),
            ListTile(
              title: Text("Login"),
              trailing: Icon(Icons.person),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => login(context)));
              },
            ),
          ],
        ),
      ),
      body: Column(children: <Widget>[
        Flexible(
            child: ListView.builder(
                itemCount: listMessages.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  return new Text(
                    ("${listMessages[index]}\n"),
                    style: TextStyle(fontSize: 23.0),
                  );
                },
                reverse: true,
                padding: EdgeInsets.all(6.0))),
        Container(
          padding: EdgeInsets.all(10.0),
          child: TextField(
            controller: msgController,
            maxLength: 255,
            style: TextStyle(fontSize: 14, height: 1.2),
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Scrivi un messaggio',
                suffixIcon: IconButton(
                    icon: Icon(Icons.send),
                    onPressed: () {
                      send();
                    })),
          ),
        ),
      ]),
    );
  }

  Widget login(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Login",
        ),
      ),
      body: Container(
          padding: new EdgeInsets.all(
              10.0), //distanza del widget dal bordo superiore.
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextField(
                keyboardType: TextInputType.number,
                controller: ipController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'IP',
                ),
              ),
              TextField(
                controller:
                    usernameController, //imposta il controller per il testo di IP.
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Username',
                ),
                maxLength: 16,
              ),
              Row(
                children: <Widget>[
                  MaterialButton(
                    onPressed: () {
                      if (socket == null) {
                        userlogin();
                      }
                      Navigator.pop(context);
                    },
                    child: const Text('LOGIN', style: TextStyle(fontSize: 13)),
                  ),
                  MaterialButton(
                    onPressed: () {
                      if (socket == null) {
                        _authorizeNow();
                      }
                      Navigator.pop(context);
                    },
                    child: Text("IMPRONTA", style: TextStyle(fontSize: 13)),
                  ),
                ],
              )
            ],
          )),
    );
  }
}
