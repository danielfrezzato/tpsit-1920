import 'dart:io';

// USE ALSO netcat 127.0.0.1 3000

// global variables

ServerSocket server;
List<ChatClient> clients = [];
List<String> utentiConnessi = <String>[];
List<String> utentiIndirizzi = <String>[];
List<String> unames = <String>[];
String username;

void main() {
  ServerSocket.bind(InternetAddress.anyIPv4, 3000).then((ServerSocket socket) {
    server = socket;
    server.listen((client) {
      handleConnection(client);
    });
  });
}

void handleConnection(Socket client) {
  print('Connection from '
      '${client.remoteAddress.address}:${client.remotePort}');
  clients.add(ChatClient(client));
  if (clients.length - 1 == 0)
    client.write("Server: Benvenuto nella chat! "
        "Non ci sono altri utenti nella chat");
  else
    client.write("Server: Benvenuto nella chat! "
        "Ci sono ${clients.length - 1} persone online:\n"
        "${utentiConnessi.toString()}\n");
}

void removeClient(ChatClient client) {
  clients.remove(client);
}

void distributeMessage(ChatClient client, String message) {
  for (ChatClient c in clients) {
    if (c != client) {
      c.write(message + "\n");
    }
  }
}

// ChatClient class for server

class ChatClient {
  Socket _socket;
  String get _address => _socket.remoteAddress.address;
  int get _port => _socket.remotePort;

  ChatClient(Socket s) {
    _socket = s;
    _socket.listen(messageHandler,
        onError: errorHandler, onDone: finishedHandler);
  }

  void messageHandler(data) {
    String message = new String.fromCharCodes(data).trim();
    unames = message.trim().split(":");
    username = unames[0];
    if (!utentiConnessi.contains(username) && !username.contains('*')) {
      utentiConnessi.insert(0, username);
      utentiIndirizzi.insert(0, "${_address}:$username");
    }
    if (username.startsWith('*')) {
      String userdis = username.substring(1);
      removeUser(userdis);
    }
    distributeMessage(this, '$message');
  }

  void errorHandler(error) {
    print('${_address}:${_port}:$username Error: $error');
    removeClient(this);
    _socket.close();
  }

  void finishedHandler() {
    String disconnected = '${_address}:${_port}:$username Disconnesso';
    print(disconnected);
    removeClient(this);
    List<String> temp = <String>[];
    temp = disconnected.split(":");
    for (int i = 0; i < utentiIndirizzi.length; i++) {
      List<String> addrname = utentiIndirizzi[i].split(":");
      String addr = addrname[0];
      String nom = addrname[1];
      if (temp[0] == addr) {
        removeUser(nom);
      }
    }
    _socket.close();
  }

  void write(String message) {
    _socket.write(message);
  }

  void removeUser(String n) {
    utentiConnessi.remove(n);
    utentiConnessi.join(', ');
  }
}
