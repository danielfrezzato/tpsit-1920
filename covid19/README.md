# covid19

Progetto Flutter per la visualizzazione e l'usabilità delle API per il covid19 

## Spiegazione progetto e funzionalità

Funzionamento base:

1)richiesta GET http al link

2)Lettura file JSON

3)Fetch Data List del JSON

``` dart
Future<List<Data>> fetchDataList() async {
  final response =
      await http.get('https://corona.lmao.ninja/v2/countries/');
  if (response.statusCode == 200) {
    final parsed = json.decode(response.body);
    return parsed.map<Data>((json) => Data.fromJson(json)).toList();
  } else {
    throw Exception('Failed to get Data');
  }
}
```


4)Lettura della lista che contiene informazioni dei paesi del JSON

``` dart
void _getData() async {
    List<Data> dataList = await fetchDataList();
    paesiDaVisualizzare.removeRange(0, paesiDaVisualizzare.length);
    setState(() {
      for (int i = 0; i < dataList.length; i++) {
        _country = dataList[i].country;
        _deaths = dataList[i].deaths.toString();
        _cases = dataList[i].cases.toString();
        _todayCases = dataList[i].todayCases.toString();
        _todayDeaths = dataList[i].todayDeaths.toString();
        totCases += dataList[i].cases;
        totDeaths += dataList[i].deaths;
        totCasesToday = dataList[i].todayCases;
        totDeathsToday += dataList[i].todayDeaths;

        Map map = {
          'paese': _country,
          'morti': _deaths,
          'casi': _cases,
          'casioggi': _todayCases,
          'mortioggi': _todayDeaths
        };
        paesiDaVisualizzare.add(map);
      }
      lunghezzaLista = paesiDaVisualizzare.length;
    });
  }
```


5)Visualizzazione paesi con dati relativi su elenco scrollabile


Funzionalità

1)Visuaizzazione di tutti i paesi

2)Cerca e visualizza il paese desiderato

3)Cerca notizie su Google relative al covid per il paese desiderato

4)Visualizza i dati relativi al mondo intero