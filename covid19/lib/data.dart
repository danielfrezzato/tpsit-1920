import 'dart:convert';
import 'package:http/http.dart' as http;


Future<List<Data>> fetchDataList() async {
  final response =
      await http.get('https://corona.lmao.ninja/v2/countries/');
  if (response.statusCode == 200) {
    // final parsed = json.decode(response.body).cast<Map<String, dynamic>>();
    // avendo una lista di json, non un oggetto json possiamo usare anche il seguente
    final parsed = json.decode(response.body);
    // print('body parsed $parsed');
    return parsed.map<Data>((json) => Data.fromJson(json)).toList();
  } else {
    throw Exception('Failed to get Data');
  }
}


class Data {


  final String country;
  final int cases;
  final int todayCases;
  final int deaths;
  final int todayDeaths;
  final int recovered;
  final int active;
  final int critical;

  Data({
    this.country, 
    this.cases, 
    this.todayCases,
    this.deaths,
    this.todayDeaths,
    this.recovered,
    this.active,
    this.critical
    });
  

  // a factory named constructor
  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      country: json['country'] as String,
      cases: json['cases'] as int,
      todayCases: json['todayCases'] as int,
      deaths: json['deaths'] as int,
      todayDeaths: json['todayDeaths'] as int,
      recovered: json['recovered'] as int,
      active: json['active'] as int,
      critical: json['critical'] as int
      );
  }

}

