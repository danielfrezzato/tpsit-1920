# Cronometro

Progetto di TPSIT di quinta con obbiettivo finale quello della realizzazione di un cronometro attraverso l'uso degli Stream in Flutter.

## Soluzione risolutiva usata

-**Stream**

-**StreamSubscription**

---

1)Usiamo gli Stream per accedere ad una fonte di dati asincroni. Questo ci permette di generare un flusso per ricevere una sequenza di eventi. Questo ci permette di utilizzare il metodo *listen*
per porci in ascolto dello Stream.

2)Quando siamo in ascolto di uno Stream possiamo utilizzare una StreamSubscription per fornire eventi ad un *listener* ed utilizzare delle operazioni di callback per gestire tali eventi.

### Classi usate per la realizzazione del progeto

1)Classe **main**

Richiama la classe principale *home* che gestisce l'interfaccia grafica e il funzionamento del cronometro. Contiene il RunApp.

2)Classe **home**

Gestisce l'interfaccia grafica con i 4 pulsanti in riga (Row)->Start,Stop,Reset e Giro.

Vi sono vari campi come lo Stream,lo StreamSubscription,variabile per gestire minuti e secondi e infine variabile e lista per gestire la lista in cui verrano memorizzati i giri.

Il costruttore della classe crea uno Stream di int con i parametri di Duration (1 secondo) e una variabile int chiamata in questo caso *numero*.

Viene poi applicato un *listen* sullo Stream e lo si assegna alla *Subscription* creata in precedenza.

Vengono cosi' incrementati i secondi

---
Il metodo start() ci permette di startare la Subscription e quindi di continuare ad incrementare il tempo del cronometro. Pulsante Start.

Il metodo stop() ci permette di stoppare la Subscription. Pulsante Stop.

Il metodo reset() ci permette di resettare la Subscription e quindi di azzerare il tempo del cronometro e di cancellare tutti gli elementi dalla lista dei giri. Pulsante Reset.

Il metodo text() ci serve per la visualizzazione del tempo tenuto dal cronometro e che poi verrà stampato a schermata.

I metodi salvaGiro() e listaGiri() ci permetteno di salvare il tempo del cronometro in un elemento della lista per poi estrarli tutti e stamparli a schermo tramite una ListView.


Il metodo build() e buildBody() gestiscono l'interfaccia grafica composta da un *Text* contenente il tempo del cronometro, una *ListView* contenente tutti i giri salvati e una Row
contente i 4 pulsanti.


