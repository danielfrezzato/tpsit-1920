import 'dart:async';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _MyHomePage createState() => _MyHomePage();
}

class _MyHomePage extends State<Home> {
  bool firstTime = false;
  String _strGiro = "";
  List<String> _myListGiri = new List();
  int _seconds = 0;
  int _minutes = 0;
  Stream<int> stream;
  StreamSubscription sub;

  _MyHomePage() {
    this.stream = Stream<int>.periodic(Duration(seconds: 1), (int numero) {
      return numero;
    });
    this.sub = this.stream.listen((data) {
      setState(() {
        this._seconds++;
      });
    });
  }

  void start() {
    firstTime = true;
    this.sub.resume();
  }

  void pausa() {
    this.sub.pause();
  }

  void reset() {
    setState(() {
      this._seconds = 0;
      this._minutes = 0;
      this.sub.pause();
      start();
      _strGiro = "";
      _myListGiri.removeRange(0, _myListGiri.length);
    });
  }

  String text() {
    String testo = "";
    if (_seconds > 59) {
      _seconds = 0;
      _minutes++;
    }
    if (_seconds < 10)
      testo = '0' + '$_seconds';
    else
      testo = '$_seconds';
    if (_minutes <= 9)
      return '0' + '$_minutes' + ' : ' + testo;
    else
      return '$_minutes' + ' : ' + testo;
  }

  void _salvaGiro() {
    _strGiro = "";
    _myListGiri.add(text());
    listaGiri();
  }

  void listaGiri() {
    for (int i = 0; i < _myListGiri.length; i++) {
      _strGiro += _myListGiri.elementAt(i) + '\n';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cronometro'),
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    if (!firstTime) {
      this.sub.pause();
      _myListGiri.add("");
    }
    return Column(
      children: <Widget>[
        Container(
          child: Text(
            text(),
            style: TextStyle(fontSize: 75),
          ),
        ),
        Container(
          height: 600,
          child: ListView(
            children: <Widget>[
              Center(
                child: Text(
                  _strGiro,
                  style: TextStyle(fontSize: 25)
                )
              )
            ],
          ),
        ),
        Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FloatingActionButton(
                backgroundColor: Colors.green,
                child: Text("Start"),
                onPressed: this.start,
              ),
              FloatingActionButton(
                backgroundColor: Colors.red,
                child: Text('Stop'),
                onPressed: this.pausa,
              ),
              FloatingActionButton(
                backgroundColor: Colors.blue,
                child: Text('Reset'),
                onPressed: this.reset,
              ),
              FloatingActionButton(
                backgroundColor: Colors.yellow,
                child: Text("Giro"),
                onPressed: () {
                  _salvaGiro();
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
