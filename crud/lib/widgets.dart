import 'package:flutter/material.dart';

import 'task.dart';
import 'task_dao.dart';

class ListCell extends StatelessWidget {
  ListCell({
    Key key,
    @required this.task,
    @required this.dao,
  }) : super(key: key);

  Task task;
  final TaskDao dao;

  _showAlertDialog(context){
    TextEditingController _myText = TextEditingController();

    showDialog(context: context,
    builder:(BuildContext context){
      _myText.text = task.message;
      return AlertDialog(
        title: Text('Modifica del Task'),
        content: TextField(
          controller: _myText,
          decoration: InputDecoration(
            hintText: 'Modifica'
            ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Cancella'), 
            onPressed: (){
              Navigator.of(context).pop();
          },
          ),
          FlatButton(
            child: Text('Salva'), 
            onPressed: ()async{
              task.message = _myText.text;
              await dao.updateTask(task);
              Navigator.of(context).pop();
            },
          )
        ],
      );
    });

  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: Key('${task.hashCode}'),
      confirmDismiss: (direction) async { // verso destra
        if (direction == DismissDirection.startToEnd) {
          _showAlertDialog(context);
          return false;
        } else {
          return true;
        }
      },
      background: Container(
        color: Colors.yellow,
        alignment: Alignment.centerLeft,
        child: Icon(Icons.edit, color: Colors.white),
      ),
      secondaryBackground: Container(
        color: Colors.red,
        alignment: Alignment.centerRight,
        child: Icon(Icons.delete_forever, color: Colors.white),
      ),
      child: ListTile(title: Text(task.message)),
      onDismissed: (direction) async { // verso sinistra
        await dao.deleteTask(task);     
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: const Text('Task cancellata'),
            duration: Duration(milliseconds: 250),
          ),
        );
      },
    );
  }
}