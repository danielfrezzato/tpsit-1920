# Client


**Soluzione risolutiva usata**

*Socket*

Usiamo un Socket per la comunicazione di una stringa scelta dall'utente al server.

**Classi usate per la realizzazione del progetto**

1)Classe **main**

Gestisce il lato client e l'invio della stringa da trasformare in maiuscolo.

Ho scelto di utilizzare un TextField in cui l'utente può specificare l'ip del server perchè client e server devono essere collegati alla stessa rete, e l'indirizzo ip del pc che fungerà
da server può variare in base alla rete.
La stringa inserita in un altro TextField dall'utente viene inviata al server che la trasforma in maiuscolo e la rispedisce al client.


---


## Server

Server TCP usato per la comunicazione con il client.

Trasforma la stringa inviata dal client in maiuscolo per poi restituirla.

File dart da far girare su macchina che funge da server.



