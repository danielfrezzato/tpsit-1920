import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Client',
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      home: MyHomePage(title: 'Client'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String ip = "";
  String inviata = "";
  String ricevuta = "";
  String connessione = "Not Connected to server!";
  void _invia() async {
    ricevuta = "";
    /*try{
      Socket socket = await Socket.connect(ip, 3000);
    }
    on Exception catch (_){
      print("throwing new error");
      setState(() {
         throw Exception(connessione="Error on server!");
      });
    }*/
    Socket socket = await Socket.connect(ip, 3000);
    setState(() {
      connessione = "Connected to server!";
    });

    socket.listen((List<int> event) {
      setState(() {
        ricevuta = ricevuta + (utf8.decode(event));
      });
    });

    socket.add(utf8.encode(inviata));

    socket.close();
  }

  @override
  Widget build(BuildContext context) {
    void _onChanged(String value) {
      inviata = '$value';
    }

    void _onChanged2(String value) {
      ip = '$value';
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
                height: 100,
                child: TextField(
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Inserisci IP server"),
                  maxLines: 1,
                  autofocus: true,
                  textAlign: TextAlign.center,
                  onChanged: (String value) {
                    _onChanged2(value);
                  },
                )),
            Container(
                height: 200,
                child: TextField(
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Inserisci messaggio"),
                  maxLines: 1,
                  autofocus: true,
                  textAlign: TextAlign.center,
                  onChanged: (String value) {
                    _onChanged(value);
                  },
                )),
            Container(
                height: 60,
                child: Text(
                  connessione,
                  style: TextStyle(fontSize: 15),
                )),
            Text(
              ricevuta,
              style: TextStyle(fontSize: 30),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _invia,
        child: Text("INVIA"),
      ),
    );
  }
}
