import 'package:flutter/material.dart';
import 'package:memory_card/pages/game_page.dart';
import 'package:memory_card/pages/splashscreen_page.dart';

var routes = <String, WidgetBuilder>{
  "/game": (BuildContext context) => GamePage(),
};

void main() => runApp(new MaterialApp(
    title: 'Memory',
    darkTheme: ThemeData(
      brightness: Brightness.dark,
    ),
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
    routes: routes));
