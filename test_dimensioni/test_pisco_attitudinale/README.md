# test_pisco_attitudinale

Test Psico Attitudinale in Flutter

## Spiegazione progetto

Schermata 1

Test con drag and drop

Schermata 2

Dati e risultati del test una volta completato come tempo(usato cronometro) ed errori con relativa possibilità di riavviare il test

### Codice utile

Creazione item draggable

``` dart
initGame() {
  items1 = [
    Card(
        lato: 50,
        delta: 100,
        n: 0), //creazione di tutti e due le liste delle liste di Items
    Card(lato: 60, delta: 125, n: 1),
    Card(lato: 70, delta: 150, n: 2),
    Card(lato: 80, delta: 175, n: 3),
    Card(lato: 90, delta: 200, n: 4),
  ];
  items2 = List<Card>.from(items1);
  items1.shuffle();
}
```

Controllo ed eventuale eliminazione items

``` dart
Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: items1.map((item) {
                                    return Container(
                                        child: Draggable<Card>(
                                      data: item, //cosa viene trascinato
                                      childWhenDragging: Container(
                                          //mentre si trascina
                                          color: Colors.black,
                                          height: item.lato,
                                          width: item.lato),
                                      feedback: Container(
                                          //una volta trascinato
                                          color: Colors.green,
                                          height: item.lato,
                                          width: item.lato),
                                      child: Container(
                                          color: Colors.green,
                                          height: item.lato,
                                          width: item.lato),
                                    ));
                                  }).toList()),
                              Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: items2.map((item) {
                                    return Container(
                                        child: DragTarget<Card>(
                                      onAccept: (data) {
                                        if (item.n == data.n) {
                                          //controllo se è giusto l'accopiamento
                                          quantita++;
                                          tempo = text();
                                          if (quantita > 4) {
                                            Navigator.push(
                                              //passo alla pgina dei risultati se il test è finito
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      PaginaRiepilogo()),
                                            );
                                          }
                                          setState(() {
                                            //rimuovo se è giusto
                                            items1.remove(data);
                                            items2.remove(item);
                                          });
                                        } else {
                                          //se è sbagliato aumento gli errori
                                          setState(() {
                                            errori++;
                                          });
                                        }
                                      },
                                      onWillAccept: (data) => true,
                                      builder: (context, acceptedItems,
                                              rejectedItems) =>
                                          Container(
                                        color: Colors.green,
                                        height: 100,
                                        width: item.delta,
                                      ),
                                    ));
                                  }).toList()),
                            ])
```