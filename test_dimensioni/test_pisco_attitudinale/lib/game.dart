import 'package:flutter/material.dart';
import 'dart:async';

List<Card> items1;
List<Card> items2;
int errori = 0;
int quantita = 0; //per controllare quanti item rimangono
String tempo = "";
int seconds = 0;
int minutes = 0;

initGame() {
  items1 = [
    Card(
        lato: 50,
        delta: 100,
        n: 0), //creazione di tutti e due le liste delle liste di Items
    Card(lato: 60, delta: 125, n: 1),
    Card(lato: 70, delta: 150, n: 2),
    Card(lato: 80, delta: 175, n: 3),
    Card(lato: 90, delta: 200, n: 4),
  ];
  items2 = List<Card>.from(items1);
  items1.shuffle();
}

class Game extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TestPsicoAttitudinale',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: MyHomePage(title: 'drag and drop'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  //cronometro per tempo
  Stream<int> stream;
  StreamSubscription sub;

  MyHomePageState() {
    this.stream = Stream<int>.periodic(Duration(seconds: 1), (int numero) {
      return numero;
    });
    this.sub = this.stream.listen((data) {
      setState(() {
        seconds++;
      });
    });
  }

  String text() {
    String testo = "";
    if (seconds > 59) {
      seconds = 0;
      minutes++;
    }
    if (seconds < 10)
      testo = '0' + '$seconds';
    else
      testo = '$seconds';
    if (minutes <= 9)
      return '0' + '$minutes' + ' : ' + testo;
    else
      return '$minutes' + ' : ' + testo;
  }

  @override
  void initState() {
    super.initState();
    initGame();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          pinned: true,
          expandedHeight: 100.0,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: Text("Test Psico Attitudinale"),
          ),
        ),
        SliverFixedExtentList(
          itemExtent: 600.0,
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Container(
                  margin: const EdgeInsets.all(10.0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Spacer(flex: 1),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: items1.map((item) {
                                    return Container(
                                        child: Draggable<Card>(
                                      data: item, //cosa viene trascinato
                                      childWhenDragging: Container(
                                          //mentre si trascina
                                          color: Colors.black,
                                          height: item.lato,
                                          width: item.lato),
                                      feedback: Container(
                                          //una volta trascinato
                                          color: Colors.green,
                                          height: item.lato,
                                          width: item.lato),
                                      child: Container(
                                          color: Colors.green,
                                          height: item.lato,
                                          width: item.lato),
                                    ));
                                  }).toList()),
                              Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: items2.map((item) {
                                    return Container(
                                        child: DragTarget<Card>(
                                      onAccept: (data) {
                                        if (item.n == data.n) {
                                          //controllo se è giusto l'accopiamento
                                          quantita++;
                                          tempo = text();
                                          if (quantita > 4) {
                                            Navigator.push(
                                              //passo alla pgina dei risultati se il test è finito
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      PaginaRiepilogo()),
                                            );
                                          }
                                          setState(() {
                                            //rimuovo se è giusto
                                            items1.remove(data);
                                            items2.remove(item);
                                          });
                                        } else {
                                          //se è sbagliato aumento gli errori
                                          setState(() {
                                            errori++;
                                          });
                                        }
                                      },
                                      onWillAccept: (data) => true,
                                      builder: (context, acceptedItems,
                                              rejectedItems) =>
                                          Container(
                                        color: Colors.green,
                                        height: 100,
                                        width: item.delta,
                                      ),
                                    ));
                                  }).toList()),
                            ])
                      ]));
            },
            childCount: 1,
          ),
        ),
      ],
    );
  }
}

class PaginaRiepilogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Risultati Test Psico Attitudinale"),
      ),
      body: Container(
        height: 600,
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                onPressed: () {
                  quantita = 0;
                  errori = 0;
                  seconds = 0;
                  minutes = 0;
                  initGame();
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.settings_backup_restore,
                ),
              ),
              Text(
                "Errori: " + errori.toString(),
                style: TextStyle(color: Colors.white, fontSize: 24),
              ),
              Text(
                "Tempo: " + tempo,
                style: TextStyle(color: Colors.white, fontSize: 24),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Card {
  //classe per le figure
  final double lato;
  final double delta;
  final double n;

  Card({this.lato, this.delta, this.n});
}
