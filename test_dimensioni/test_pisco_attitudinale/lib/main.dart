import 'package:flutter/material.dart';

import 'game.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TestPsicoAttitudinale',
      debugShowCheckedModeBanner: false,
      home: Game(),
    );
  }
}
